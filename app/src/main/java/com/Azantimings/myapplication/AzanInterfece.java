package com.Azantimings.myapplication;

import android.content.Intent;
import android.widget.EditText;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static android.content.Intent.getIntent;

public interface AzanInterfece {

@GET("timingsByCity?city&country&method&school")
Call<ROOT> getAzantime(@Query("city") String city,
                       @Query("country")String country,
                       @Query("method")int method,
                       @Query("school")int school);
}
