package com.Azantimings.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DATA {
    @SerializedName("timings")
    @Expose
    private TIMINGS timings;
    @SerializedName("date")
    @Expose
    private DATE date;
    @SerializedName("meta")
    @Expose
    private META meta;

    public META getMeta() {
        return meta;
    }

    public void setMeta(META meta) {
        this.meta = meta;
    }

    public DATE getDate() {
        return date;
    }

    public void setDate(DATE date) {
        this.date = date;
    }

    public TIMINGS getTimings() {
        return timings;
    }

    public void setTimings(TIMINGS timings) {
        this.timings = timings;
    }
}
