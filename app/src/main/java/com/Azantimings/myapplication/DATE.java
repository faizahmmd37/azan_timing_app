package com.Azantimings.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DATE {
    @SerializedName("gregorian")
    @Expose
    private GREGORIAN gregorian;
    @SerializedName("hijri")
    @Expose
    private HIJRI hijri;

    public HIJRI getHijri() {
        return hijri;
    }

    public void setHijri(HIJRI hijri) {
        this.hijri = hijri;
    }

    public GREGORIAN getGregorian() {
        return gregorian;
    }

    public void setGregorian(GREGORIAN gregorian) {
        this.gregorian = gregorian;
    }
}
