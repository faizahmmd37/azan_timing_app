package com.Azantimings.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class DisplayActivity extends AppCompatActivity {
TextView displayedittext;
    FloatingActionButton clearfloat,sharefloat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        clearfloat=findViewById(R.id.clearbtn);
        sharefloat=findViewById(R.id.sendbtn);
        displayedittext=findViewById(R.id.displaytextview);
        Intent intent=getIntent();
        String data=intent.getStringExtra("resultdatakey").toString();
        displayedittext.setText(data.toString());

        clearfloat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayedittext.setText("");
            }
        });

        sharefloat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT,displayedittext.getText().toString());
                intent.setType("text/plain");
                intent.createChooser(intent,"share details via");
                startActivity(intent);
            }
        });


    }
}