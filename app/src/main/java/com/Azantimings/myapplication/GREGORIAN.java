package com.Azantimings.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GREGORIAN {
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("weekday")
    @Expose
    private WEEKDAY weekday;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public WEEKDAY getWeekday() {
        return weekday;
    }

    public void setWeekday(WEEKDAY weekday) {
        this.weekday = weekday;
    }
}
