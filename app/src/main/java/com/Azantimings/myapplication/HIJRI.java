package com.Azantimings.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HIJRI {
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("weekday")
    @Expose
    private WEEKDAY2 weekday2;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public WEEKDAY2 getWeekday2() {
        return weekday2;
    }

    public void setWeekday2(WEEKDAY2 weekday2) {
        this.weekday2 = weekday2;
    }
}
