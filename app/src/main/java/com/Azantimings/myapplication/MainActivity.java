package com.Azantimings.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.IllegalFormatCodePointException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
EditText editText;
Button button;
Spinner spinner,spinner2,spinner3;

    SharedPreferences sharedPreferences;
    int spinner2position;
    int spinnerschool;
    int spinnermethodposition;

DrawerLayout drawerLayout;
NavigationView navigationView;
Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText=findViewById(R.id.edittext);


        drawerLayout=findViewById(R.id.drawerlayout);
        navigationView=findViewById(R.id.navigationview);
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle actionBarDrawerToggle=new ActionBarDrawerToggle(MainActivity.this,drawerLayout,toolbar,R.string.open,R.string.close);
drawerLayout.addDrawerListener(actionBarDrawerToggle);
actionBarDrawerToggle.syncState();

        button=findViewById(R.id.button);
        spinner=findViewById(R.id.spinner);
        spinner2=findViewById(R.id.spinner2);
        spinner3=findViewById(R.id.spinner3);
        spinner.setClickable(true);
        //spinner2.setClickable(true);
        spinner.setSelection(1);
        //spinner2.setSelection(0);
        spinner3.setSelection(0);



button.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        String city=editText.getText().toString();
        spinnermethodposition=spinner.getSelectedItemPosition();
       final String methodname= spinner.getSelectedItem().toString();
        String spinnercountry=spinner2.getSelectedItem().toString();
       spinnerschool=spinner3.getSelectedItemPosition();



        spinner2position=spinner2.getSelectedItemPosition();
        sharedPreferences=getSharedPreferences("key",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putInt("key",spinner2position);
        editor.putInt("key1",spinnerschool);
        editor.putInt("key2",spinnermethodposition);
        editor.apply();
        editor.commit();




        Retrofit retrofit=new Retrofit.Builder().baseUrl("http://api.aladhan.com/v1/").addConverterFactory(GsonConverterFactory.create()).build();
        AzanInterfece azanInterfece=retrofit.create(AzanInterfece.class);
        Call<ROOT> call = azanInterfece.getAzantime(city,spinnercountry,spinnermethodposition,spinnerschool);
        call.enqueue(new Callback<ROOT>() {
            @Override
            public void onResponse(Call<ROOT> call, Response<ROOT> response) {
                if (response.body()!=null){
                    ROOT list=response.body();

                    String A="Date :" +list.getData().getDate().getGregorian().getDate().toString();
                    String B="Weekday :" +list.getData().getDate().getGregorian().getWeekday().getEn().toString();

                    String X="Date :"+list.getData().getDate().getHijri().getDate().toString();
                    String Y="Weekday :"+list.getData().getDate().getHijri().getWeekday2().getEn().toString();
                    String Z=""+list.getData().getDate().getHijri().getWeekday2().getAr().toString();

                    String P=""+list.getData().getMeta().getLatitude();
                    String Q=""+list.getData().getMeta().getLongitude();
                    String R=""+editText.getText().toString();

                    String one="Fajr :"+list.getData().getTimings().getFajr().toString();
                    String two="Sunrise :"+list.getData().getTimings().getSunrise().toString();
                    String three="Dhuhr :"+list.getData().getTimings().getDhuhr().toString();
                    String four="Asr :"+list.getData().getTimings().getAsr().toString();
                    String five="Sunset :"+list.getData().getTimings().getSunset().toString();
                    String six="Maghrib :"+list.getData().getTimings().getMagrib().toString();
                    String seven="Isha :"+list.getData().getTimings().getIsha().toString();
                    String eight="Imsak :"+list.getData().getTimings().getImsak().toString();
                    String nine="Midnight :"+list.getData().getTimings().getMidnight().toString();

                    String result="";
                    result+="Gregorian"+"\n";
                    result+="" +A+"\n";
                    result+=""+B+"\n\n";
                    result+="Hijri"+"\n";
                    result+=""+X+"\n";
                    result+=""+Y+Z +"\n\n" ;
                    result+="Information provided from"+"\n";
                    result+=""+methodname+"\n\n";
                    result+="latitude :"+P+","+"longitude :"+Q+"\n";
                    result+="City name :"+R+"\n\n";
                    result+= ""+one+"\n\n";
                    result+= ""+two+"\n\n";
                    result+= ""+three+"\n\n";
                    result+= ""+four+"\n\n";
                    result+= ""+five+"\n\n";
                    result+= ""+six+"\n\n";
                    result+= ""+seven+"\n\n";
                    result+= ""+eight+"\n\n";
                    result+= ""+nine+"\n\n";

Intent sendresultintent=new Intent(MainActivity.this,DisplayActivity.class);
sendresultintent.putExtra("resultdatakey",result);
startActivity(sendresultintent);



                }else
                {
                    Toast.makeText(MainActivity.this,"Something went wrong",Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<ROOT> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Something went wrong",Toast.LENGTH_LONG).show();
            }
        });
    }
});

navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.aboutitem:
                AlertDialog.Builder alert =new AlertDialog.Builder(MainActivity.this);
                alert.setCancelable(true);
                alert.setTitle("About Us");
                alert.setMessage(R.string.aboutus);
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alert.create();
                alert.show();
                break;
            case R.id.termsofserviceitem:
                AlertDialog.Builder alert2=new AlertDialog.Builder(MainActivity.this);
                alert2.setCancelable(true);
                alert2.setTitle("Terms of Service");
                alert2.setMessage(R.string.termsofservice);
                alert2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alert2.create();
                alert2.show();
                break;
            case R.id.contactitem:
                Intent intent=new Intent();
                intent.setAction(Intent.ACTION_SEND);
                String[] recipients={"faizahmmd37@gmail.com"};
                String[] cc_recipient={"nightowlstudioz@gmail.com"};
                intent.putExtra(Intent.EXTRA_CC,cc_recipient);
                intent.putExtra(Intent.EXTRA_EMAIL,recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT,"Contact us form");
                intent.putExtra(Intent.EXTRA_TEXT,"Write your message here...");

                intent.setType("text/html");
                intent.setPackage("com.google.android.gm");
                startActivity(Intent.createChooser(intent,"send email"));
                break;
            case R.id.shareitem:
                Intent intent2=new Intent();
                intent2.setAction(Intent.ACTION_SEND);
                final String apppackagename="com.Azantimings.myapplication";
                intent2.putExtra(Intent.EXTRA_TEXT,"https://play.google.com/store/apps/details?id="+apppackagename);
                intent2.setType("text/plain");
                Intent.createChooser(intent2,"Share via");
                startActivity(intent2);
                break;
            case R.id.rateus:
                final String packagename="com.Azantimings.myapplication";
                Uri uri=Uri.parse("market://details?id="+packagename);
                Intent intent3=new Intent(Intent.ACTION_VIEW,uri);
                try {
                    startActivity(intent3);
                }
                catch (ActivityNotFoundException e){
                    Toast.makeText(MainActivity.this,"Something went wrong",Toast.LENGTH_LONG).show();
                }

                break;

        }

        return true;
    }
});

    }

    @Override
    public void onBackPressed() {

        //super.onBackPressed();
        CallAlertBAR();

    }
    public void CallAlertBAR(){
        AlertDialog.Builder alertBar=new AlertDialog.Builder(this);
        alertBar.setTitle("EXIT");
        alertBar.setMessage("Do you really want to exit?");
        alertBar.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertBar.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                System.exit(1);
            }
        });
        alertBar.create();
        alertBar.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences=getSharedPreferences("key",MODE_PRIVATE);
        int ee=sharedPreferences.getInt("key",spinner2position);
        int eee=sharedPreferences.getInt("key1",spinnerschool);
        int eeee=sharedPreferences.getInt("key2",spinnermethodposition);
        spinner.setSelection(eeee);
        spinner2.setSelection(ee);
        spinner3.setSelection(eee);


    }
}