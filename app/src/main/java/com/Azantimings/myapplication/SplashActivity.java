package com.Azantimings.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {
    ImageView img1;
    TextView img2;
    Animation top,bottom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        img1=findViewById(R.id.imageview1);
        img2=findViewById(R.id.imageview2);
        top= AnimationUtils.loadAnimation(SplashActivity.this,R.anim.icon);
        bottom=AnimationUtils.loadAnimation(SplashActivity.this,R.anim.title);
        img1.setAnimation(top);
        img2.setAnimation(bottom);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Handler handler=new Handler();
handler.postDelayed(new Runnable() {
    @Override
    public void run() {
        Intent intent=new Intent(SplashActivity.this,MainActivity.class);
        startActivity(intent);
    }
},2000);
    }
}