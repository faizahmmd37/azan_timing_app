package com.Azantimings.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TIMINGS {
    @SerializedName("Fajr")
    @Expose
    private String Fajr;
    @SerializedName("Sunrise")
    @Expose
    private String Sunrise;
    @SerializedName("Dhuhr")
    @Expose
    private String Dhuhr;
    @SerializedName("Asr")
    @Expose
    private String Asr;
    @SerializedName("Sunset")
    @Expose
    private String Sunset;
    @SerializedName("Maghrib")
    @Expose
    private String Magrib;
    @SerializedName("Isha")
    @Expose
    private String Isha;
    @SerializedName("Imsak")
    @Expose
    private String Imsak;
    @SerializedName("Midnight")
    @Expose
    private String Midnight;

    public String getFajr() {
        return Fajr;
    }

    public void setFajr(String fajr) {
        Fajr = fajr;
    }

    public String getSunrise() {
        return Sunrise;
    }

    public void setSunrise(String sunrise) {
        Sunrise = sunrise;
    }

    public String getDhuhr() {
        return Dhuhr;
    }

    public void setDhuhr(String dhuhr) {
        Dhuhr = dhuhr;
    }

    public String getAsr() {
        return Asr;
    }

    public void setAsr(String asr) {
        Asr = asr;
    }

    public String getSunset() {
        return Sunset;
    }

    public void setSunset(String sunset) {
        Sunset = sunset;
    }

    public String getMagrib() {
        return Magrib;
    }

    public void setMagrib(String magrib) {
        Magrib = magrib;
    }

    public String getIsha() {
        return Isha;
    }

    public void setIsha(String isha) {
        Isha = isha;
    }

    public String getImsak() {
        return Imsak;
    }

    public void setImsak(String imsak) {
        Imsak = imsak;
    }

    public String getMidnight() {
        return Midnight;
    }

    public void setMidnight(String midnight) {
        Midnight = midnight;
    }
}
